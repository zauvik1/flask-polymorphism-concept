from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:12345@localhost:3306/polymorphism'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class DataCatalog(db.Model):
    __tablename__ = 'data_catalog'
    
    id = db.Column(db.Integer, primary_key=True)
    geom = db.Column(db.String(255))  # Assuming geom as string for simplicity
    table_name = db.Column(db.String(50))
    fk_id = db.Column(db.Integer, nullable=False)

class FireEvents(db.Model):
    __tablename__ = 'fire_events'
    
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date)
    severity = db.Column(db.Integer)

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Policy(db.Model):
    __tablename__ = 'policy'
    
    id = db.Column(db.Integer, primary_key=True)
    policy_name = db.Column(db.String(255))

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}


@app.route('/sample')
def sample_data():
    data_catalogs = DataCatalog.query.all()
    results = []

    for data in data_catalogs:
        original_string = data.table_name
        formatted_string = original_string.replace("_", " ").title().replace(" ", "")
        related_data = globals()[formatted_string].query.get(data.fk_id)
        
        # Assuming you want to fetch only a few attributes for the sake of this example
        results.append({
            "id": data.id,
            "geom": data.geom,
            "table_name": data.table_name,
            "variable_data": related_data.as_dict()
        })
    return jsonify(results)

if __name__ == '__main__':
    with app.app_context():
        #####
        # DATABASE SCHEMA, RUN ONCE ONLY.
        #####

        # db.create_all()
        
        # fire_event = FireEvents(date='2022-01-01', severity=5)
        # policy = Policy(policy_name='Forest Protection')
        
        # db.session.add(fire_event)
        # db.session.add(policy)
        # db.session.commit()
        
        # data1 = DataCatalog(geom='some_geometry_data', table_name='fire_events', fk_id=fire_event.id)
        # data2 = DataCatalog(geom='some_geometry_data', table_name='policy', fk_id=policy.id)
        
        # db.session.add(data1)
        # db.session.add(data2)
        # db.session.commit()


        #####
        # END SCHEMA
        ####

        app.run(debug=True)
